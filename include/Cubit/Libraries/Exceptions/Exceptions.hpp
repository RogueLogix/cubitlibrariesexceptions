#pragma once

#include <stack>
#include <string>
#include <vector>
#include <memory>

namespace Cubit::Libraries::Exceptions {
    
    class StackTraceRecorder {
    public:
        const char* const function;
        const char* const file;
        int line;
        
        StackTraceRecorder(const char* function, const char* file, int line);
        
        ~StackTraceRecorder();
        
        void updateStackTraceLine(int line);
    };
    
    class StackTraceElement {
    public:
        const char* const function;
        const char* const file;
        const int line;
        
        StackTraceElement(StackTraceRecorder* recorder);
        
        StackTraceElement(StackTraceElement& element);
    };

#ifdef Debug
#define CUBIT_STACKTRACE \
auto& _func_ = __func__;\
std::unique_ptr<Cubit::Libraries::Exceptions::StackTraceRecorder> cubitStackTraceUnwindDeletor\
(new Cubit::Libraries::Exceptions::StackTraceRecorder(_func_, __FILE__, __LINE__ - 1));
#define CUBIT_RESTACKTRACE cubitStackTraceUnwindDeletor->updateStackTraceLine(__LINE__);
#else
#define CUBIT_STACKTRACE auto& _func_ = __func__;
#define CUBIT_RESTACKTRACE
#endif
#define CUBIT_EXCEPTION_INFO _func_, __FILE__, __LINE__
    
    class ErrorBase : public std::exception {
        std::vector<std::shared_ptr<StackTraceElement>> stacktrace;
        std::string message;
        std::string errorType;
        std::string printMessage;
    protected:
        bool fatal;
    public:
        ErrorBase(const char* function, const char* file, int linenum, std::string message, std::string errorType);
        
        ErrorBase(const char* function, const char* file, int linenum, std::string message, std::string errorType,
                  bool fatal);
        
        std::string getStackTrace();
        
        const char* what();
    };
    
    class Error : public ErrorBase {
    public:
        Error(const char* function, const char* file, int linenum, std::string message, std::string errorType);
    };
    
    class FileNotFound : public Error {
    public:
        FileNotFound(const char* function, const char* file, int linenum, std::string message);
    };
    
    class InvalidRecursion : public Error {
    public:
        InvalidRecursion(const char* function, const char* file, int linenum, std::string message);
    };
    
    class InvalidArgument : public Error {
    public:
        InvalidArgument(const char* function, const char* file, int linenum, std::string message);
        
        InvalidArgument(const char* function, const char* file, int linenum, std::string message,
                        std::string errorType);
    };
    
    class InvalidState : public Error {
    public:
        InvalidState(const char* function, const char* file, int linenum, const std::string& message,
                     std::string errorType = "InvalidState");
        
    };
    
    class InvalidAsyncState : public InvalidState {
    public:
        InvalidAsyncState(const char* function, const char* file, int linenum, const std::string& message,
                          std::string errorType = "InvalidAsyncState");
        
    };
    
    class NullPointer : public Error {
    public:
        NullPointer(const char* function, const char* file, int linenum, std::string message);
    };
    
    class FatalError : public ErrorBase {
    public:
        FatalError(const char* function, const char* file, int linenum, std::string message, std::string errorType);
    };
    
    class FatalInitFailure : public FatalError {
    public:
        FatalInitFailure(const char* function, const char* file, int linenum, std::string message);
    };
    
    std::string getCurrentStackTrace();
}